import { Injectable } from '@angular/core';
import {AuthenticationService} from "../../../gs-api/src/services";
import {AccountService} from "./account.service";
import {AuthenticationRequest} from "../../../gs-api/src/models/authentication-request";
import {AuthenticationResponse} from "../../../gs-api/src/models/authentication-response";
import {Observable} from "rxjs";
import {delay, map} from "rxjs/operators";
import {WebStorageService} from "./web-storage.service";
import {Router} from "@angular/router";
import {of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthJwtService {

  constructor(
      private authenticationService: AuthenticationService,
      private accountService: AccountService,
      private webStorageService: WebStorageService,
      private router: Router
  ) { }
  
  login(authenticationRequest: AuthenticationRequest | {}, remember: boolean): Observable<void> {
    return this.authenticationService.authenticate(authenticationRequest)
        .pipe(map(response => this.authenticationSuccess(response, remember)));
  }

  authenticationSuccess(authenticationResponse: AuthenticationResponse, remember: boolean): void {
    this.webStorageService.storeToken(authenticationResponse.accessToken, remember);
    this.accountService.identity(true).subscribe();
    // const expirationTime = new Date(new Date().getTime() + (60 * 60 * 5));
    // this.logoutTimer(expirationTime.getTime());
  }

  getToken(): string{
      return this.webStorageService.getToken();
  }

  isAuthentication(): boolean {
      if (this.webStorageService.getToken()) {
          return true;
      }
      this.router.navigate(['login']);
      return false;
  }

  private logoutTimer(expirationTime: number): void {
      of(true).pipe(delay(expirationTime)).subscribe({
          next: _ => {
              this.webStorageService.clearToken();
              this.accountService.authentication(null);
              this.router.navigate(['login']);
          },
          error: err => {

          }
      });
  }

  logout(): Observable<void> {
      return new Observable(observer => {
          this.webStorageService.clearToken();
          observer.complete();
      })
  }

  logoutWithoutObserver(): void {
      this.webStorageService.clearToken();
      this.accountService.authentication(null);
      this.router.navigate(['login']);
  }


}
