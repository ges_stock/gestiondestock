import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable} from "rxjs";
import {AuthJwtService} from "./auth-jwt.service";

@Injectable({
  providedIn: 'root'
})
export class UserRouterAccessService implements CanActivate{

  constructor(
      private authJwtService: AuthJwtService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable <boolean | UrlTree> | Promise <boolean | UrlTree> {
    return this.authJwtService.isAuthentication();
  }

}
