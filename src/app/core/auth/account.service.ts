import { Injectable } from '@angular/core';
import {UtilisateursService} from "../../../gs-api/src/services";
import {ReplaySubject} from "rxjs";
import {UtilisateurDto} from "../../../gs-api/src/models/utilisateur-dto";
import {Observable} from "rxjs";
import {catchError} from "rxjs";
import {of} from "rxjs";
import {shareReplay, tap} from "rxjs/operators";
import {ChangerMotDePasseUtilisateurDto} from "../../../gs-api/src/models/changer-mot-de-passe-utilisateur-dto";

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private userIdentity: UtilisateurDto | null = null;
  private authenticationState = new ReplaySubject<UtilisateurDto | null>(1);
  private accountCache$?: Observable<UtilisateurDto | null>;

  constructor(
      private utilisateurService: UtilisateursService
  ) { }

  private getCurrentUser(): Observable<UtilisateurDto> {
    return this.utilisateurService.currentUser();
  }

  identity(force?: boolean): Observable<UtilisateurDto | null> {
    if (!this.accountCache$ || force || !this.isAuthenticated()) {
      this.accountCache$ = this.getCurrentUser().pipe(
          catchError(() => of(null)),
      tap((account: UtilisateurDto | null) => {
        this.authentication(account);
      }),
      shareReplay()
      )
    }
    return this.accountCache$
  }

  private isAuthenticated(): boolean {
    return this.userIdentity !== null;
  }

  authentication(identity: UtilisateurDto | null): void {
    this.userIdentity = identity;
    this.authenticationState.next(this.userIdentity);
  }

  getAuthenticationState(): Observable<UtilisateurDto | null> {
    return this.authenticationState.asObservable();
  }

  changePassword(changePassword: ChangerMotDePasseUtilisateurDto): Observable<UtilisateurDto> {
    return this.utilisateurService.changePassword(changePassword);
  }

}
