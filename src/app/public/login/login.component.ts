import { Component, OnInit } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {AuthJwtService} from "../../core/auth/auth-jwt.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  errorMessage: string | undefined;
  remember: boolean = false;

  loginForm = this.fb.group({
    login: [],
    password: [],
  });

  constructor(
      private fb: FormBuilder,
      private authJwtService: AuthJwtService,
      private router: Router
  ) { }

  ngOnInit(): void {
    document.body.className="hold-transition login-page";
  }

  login(): void {
    const loginRequest = this.loginForm.value;
    window.console.log(loginRequest);
    this.authJwtService.login(loginRequest, this.remember).subscribe({
        next: res => {
          this.router.navigate(['']);
        },
        error: err => {
          this.errorMessage = err?.error?.errorMessage;
        }
    })
  }

}
