import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {AccountService} from "../../core/auth/account.service";
import {UtilisateurDto} from "../../../gs-api/src/models/utilisateur-dto";
import {ChangerMotDePasseUtilisateurDto} from "../../../gs-api/src/models/changer-mot-de-passe-utilisateur-dto";
import {Router} from "@angular/router";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

    account: UtilisateurDto | null = {};
    errorMessage: string = '';

  changePasswordForm = this.fb.group(
    {
      oldPassword: ['', [Validators.required]],
      newPassword: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]]
    });

  constructor(
      private fb: FormBuilder,
      private accountService: AccountService,
      private router: Router
  ) { }

  ngOnInit(): void {
    document.body.className="hold-transition login-page";
    this.accountService.getAuthenticationState().subscribe(account => {this.account = account});
  }

  setOldPassword(): void {
      this.changePasswordForm.patchValue({
          oldPassword: "som3R@nd0mP@$$word",
      })
  }

    changePassword(): void {
        const changePasswordDto = this.createChangePasswordForm();
        this.accountService.changePassword(changePasswordDto).subscribe({next: res => {
             this.router.navigate(['']) ;
         },
          error: error => {
            this.errorMessage = error?.error?.errorMessage;
          }
        })
    }

    createChangePasswordForm(): ChangerMotDePasseUtilisateurDto {
        return {
            id: this.account?.id,
            motDePasse: this.changePasswordForm.get('newPassword')?.value ?? undefined,
            confirmMotDePasse: this.changePasswordForm.get('confirmPassword')?.value ?? undefined,
        }
    }

}
