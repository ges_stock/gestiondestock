import { Component, OnInit } from '@angular/core';
import {EntrepriseDto} from "../../../gs-api/src/models/entreprise-dto";
import {AdresseDto} from "../../../gs-api/src/models/adresse-dto";
import {EntreprisesService} from "../../../gs-api/src/services";
import {Router} from "@angular/router";
import {AuthenticationRequest} from "../../../gs-api/src/models/authentication-request";
import {AuthJwtService} from "../../core/auth/auth-jwt.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  entrepriseDto: EntrepriseDto = {};
  adresseDto: AdresseDto = {};
  errorMsg: Array<string> = [];
  isSaving = false;

  constructor(
      private entrepriseService: EntreprisesService,
      private router: Router,
      private authJwtService: AuthJwtService
  ) { }

  ngOnInit(): void {
    document.body.className="hold-transition register-page";
  }

  createEntreprse(): void {
    this.isSaving = true;
    this.entrepriseDto.adresse = this.adresseDto;
    this.entrepriseService.save(this.entrepriseDto).subscribe({
      next: res => {
        this.isSaving = false;
        this.connectUser();
      },
      error: error => {
        this.errorMsg = error.error.errors;
        this.isSaving = false;
      }
    });
  }

  connectUser(): void {
    const authentificationRequest: AuthenticationRequest = {
      login: this.entrepriseDto.email,
      password: "som3R@nd0mP@$$word"
    };
    this.authJwtService.login(authentificationRequest, false).subscribe({
      next: res => {
        this.router.navigate(['change-password']);
      }
    });
  }

}
