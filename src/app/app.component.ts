import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <ngx-loading-bar></ngx-loading-bar>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
  title = 'gestiondestock';
}
