import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ButtonActionsComponent} from "../components/button-actions/button-actions.component";
import {SharedModule} from "../../shared/shared.module";


@NgModule({
  declarations: [
    ButtonActionsComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    SharedModule,
    ButtonActionsComponent
  ]
})
export class SharedMemberModule { }
