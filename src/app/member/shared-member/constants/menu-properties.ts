import {Sidebar} from "../../layouts/sidebar/sidebar.model";

export const menuProperties: Array<Sidebar> = [
    {
        id: '1',
        title: 'Tableau de bord',
        icon: 'fas fa-chart-line',
        url: '',
        active: true,
        subMenu: [
            {
                id: '11',
                title: 'Vue d\'ensemble',
                icon: 'fas fa-chart-pie',
                url: ''
            },
            {
                id: '12',
                title: 'Statistique',
                icon: 'fas fa-chart-bar',
                url: 'statistique'
            }
        ]
    },
    {
        id: '2',
        title: 'Artilces',
        icon: 'fas fa-boxes',
        url: '',
        subMenu: [
            {
                id: '21',
                title: 'Articles',
                icon: 'fas fa-chart-pie',
                url: 'articles'
            },
            {
                id: '22',
                title: 'Mouvement de stock',
                icon: 'fas fa-exchange',
                url: 'mvtstk'
            }
        ]
    },
    {
        id: '3',
        title: 'Clients',
        icon: 'fas fa-users',
        url: '',
        subMenu: [
            {
                id: '31',
                title: 'Clients',
                icon: 'fas fa-userse',
                url: 'clients'
            },
            {
                id: '32',
                title: 'Commandes Clients',
                icon: 'fas fa-shopping-basket',
                url: 'commandesclients'
            }
        ]
    },
    {
        id: '4',
        title: 'Fournisseurs',
        icon: 'fas fa-users',
        url: '',
        subMenu: [
            {
                id: '41',
                title: 'Fournisseurs',
                icon: 'fas fa-userse',
                url: 'fournisseurs'
            },
            {
                id: '42',
                title: 'Commandes Fournisseurs',
                icon: 'fas fa-truck',
                url: 'commandesfournisseurs'
            }
        ]
    },
    {
        id: '5',
        title: 'Parametrages',
        icon: 'fas fa-cogs',
        url: '',
        subMenu: [
            {
                id: '51',
                title: 'Categories',
                icon: 'fas fa-tools',
                url: 'categories'
            },
            {
                id: '52',
                title: 'Utilsateurs',
                icon: 'fas fa-users-cog',
                url: 'utilsateurs'
            }
        ]
    }
];
