import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {menuProperties} from "../../shared-member/constants/menu-properties";
import {AccountService} from "../../../core/auth/account.service";
import {UtilisateurDto} from "../../../../gs-api/src/models/utilisateur-dto";
import {Subscription} from "rxjs";

import * as $ from 'jquery';
import * as AdminLte from 'admin-lte';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnDestroy, AfterViewInit {

  menus = menuProperties;
  account: UtilisateurDto | null = {};
  authSubscription: Subscription = new Subscription();

  constructor(
      private accountService: AccountService
  ) { }

  ngOnDestroy(): void {
    this.authSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.accountService.identity().subscribe();
    this.authSubscription.add(this.accountService.getAuthenticationState().subscribe(account => {this.account = account}));
  }

  ngAfterViewInit(): void {
    $('[data-widget="treeview"]').Treeview('init');
    // $('[data-widget="pushmenu"]').each(() => {
    //   AdminLte.Layout._jQueryInterface.call($('body'));
    //   AdminLte.PushMenu._jQueryInterface.call($('[data-widget="pushmenu"]'));
    // });
  }

}
