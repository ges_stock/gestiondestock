import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AccountService} from "../../../core/auth/account.service";
import {AuthJwtService} from "../../../core/auth/auth-jwt.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  constructor(
      private router: Router,
      private accountService: AccountService,
      private authJwtService: AuthJwtService
  ) { }

  logout(): void {
    this.authJwtService.logout().subscribe({complete: () => {
      this.accountService.authentication(null);
      this.router.navigate(['login']);
      }});
  }

}
