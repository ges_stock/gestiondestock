import {Routes} from "@angular/router";
import {HeaderComponent} from "./header/header.component";
import {SidebarComponent} from "./sidebar/sidebar.component";
import {FooterComponent} from "./footer/footer.component";

export const layoutsRouters: Routes = [
    {
        path: '',
        component: HeaderComponent,
        outlet: 'header'
    },
    {
        path: '',
        component: SidebarComponent,
        outlet: 'sidebar'
    },
    {
        path: '',
        component: FooterComponent,
        outlet: 'footer'
    }
];
