import { Component, OnInit } from '@angular/core';
import {FournisseurDto} from "../../../../../../gs-api/src/models/fournisseur-dto";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {SupplierService} from "../service/supplier.service";

@Component({
  selector: 'app-suppliers-delete',
  templateUrl: './suppliers-delete.component.html',
  styleUrls: ['./suppliers-delete.component.scss']
})
export class SuppliersDeleteComponent {

  supplier: FournisseurDto = {};
  errorMsg: string = '';
  isVisible = false;

  constructor(
      private activeModal: NgbActiveModal,
      private supplierService: SupplierService,
  ) { }

  cancel() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id : number): void{
    this.isVisible = true;
    this.supplierService.deleteSupplier(id).subscribe({next: res => {
        this.activeModal.close("success");
        this.isVisible = false;
      }, error: err => {
        this.errorMsg = err.error.error;
        this.isVisible = false;
      }});
  }

}
