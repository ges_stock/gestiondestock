import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {FournisseurDto} from "../../../../../../gs-api/src/models/fournisseur-dto";
import {SupplierService} from "../service/supplier.service";

@Component({
  selector: 'app-suppliers-list',
  templateUrl: './suppliers-list.component.html',
  styleUrls: ['./suppliers-list.component.scss']
})
export class SuppliersListComponent implements OnInit {

  suppliers$: Observable<FournisseurDto[] | []> | undefined;

  constructor(
      private router: Router,
      private supplierService: SupplierService,
  ) { }

  ngOnInit(): void {
    this.refresh();
  }

  newSupplier(): void {
    this.router.navigate(['fournisseurs/new']);
  }

  refresh(): void {
    this.suppliers$ = this.supplierService.findAll();
  }

  trackBy(index: number, item: FournisseurDto): number {
    return item.id!;
  }

  handleDelete(event: any){
    if (event === 'supplier-delete') {
      this.refresh();
    }
  }

}
