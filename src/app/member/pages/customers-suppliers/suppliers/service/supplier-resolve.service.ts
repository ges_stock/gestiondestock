import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs";
import {mergeMap} from "rxjs/operators";
import {EMPTY} from "rxjs";
import {of} from "rxjs";
import {FournisseurDto} from "../../../../../../gs-api/src/models/fournisseur-dto";
import {SupplierService} from "./supplier.service";


@Injectable({providedIn: 'root'})
export class SupplierResolveService  implements Resolve<FournisseurDto>{

    constructor(private service: SupplierService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable <FournisseurDto> | Promise<FournisseurDto> | FournisseurDto {
        const id = route.params['id'];
        if (id) {
            return this.service.findById(id).pipe(
                mergeMap((fournisseur: FournisseurDto) => {
                    if (fournisseur) {
                        return of(fournisseur);
                    }
                    else {
                        return EMPTY;
                    }
                })
            );
        }
        return of({});
    }

}
