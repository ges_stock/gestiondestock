import { Injectable } from '@angular/core';
import {Observable, of} from "rxjs";
import {FournisseurService} from "../../../../../../gs-api/src/services/fournisseur.service";
import {FournisseurDto} from "../../../../../../gs-api/src/models";

@Injectable({
  providedIn: 'root'
})
export class SupplierService {

  constructor(
      private fournisseursService: FournisseurService
  ) { }

  save(fournisseurDto: FournisseurDto): Observable<FournisseurDto> {
    return this.fournisseursService.save(fournisseurDto);
  }

  findAll(): Observable<FournisseurDto[]> {
    return this.fournisseursService.findAll();
  }

  findById(idFournisseur: number): Observable<FournisseurDto> {
    return this.fournisseursService.findById(idFournisseur);
  }

  deleteSupplier(idFournisseur: number): Observable<any> {
    if (idFournisseur) {
      return this.fournisseursService.delete(idFournisseur);
    }
    return of();
  }

}
