import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {NewCustomerSupplierComponent} from "../../../components/new-customer-supplier/new-customer-supplier.component";
import {SuppliersListComponent} from "./suppliers-list/suppliers-list.component";
import {SupplierResolveService} from "./service/supplier-resolve.service";

const routes: Routes = [
  {
    path: '',
    component: SuppliersListComponent
  },
  {
    path: 'new',
    data: {
      origin: 'fournisseur'
    },
    component: NewCustomerSupplierComponent
  },
  {
    path: ':id/edit',
    data: {
      origin: 'fournisseur'
    },
    component: NewCustomerSupplierComponent,
    resolve: {
      clientFournisseur: SupplierResolveService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuppliersRoutingModule { }
