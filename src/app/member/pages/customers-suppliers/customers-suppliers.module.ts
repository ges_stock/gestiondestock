import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomersSuppliersRoutingModule } from './customers-suppliers-routing.module';
import {SuppliersListComponent} from "./suppliers/suppliers-list/suppliers-list.component";
import {CustomersListComponent} from "./customers/customers-list/customers-list.component";
import {CustomersDeleteComponent} from "./customers/customers-delete/customers-delete.component";
import {SuppliersDeleteComponent} from "./suppliers/suppliers-delete/suppliers-delete.component";
import {CustomerSupplierItemComponent} from "../../components/customer-supplier-item/customer-supplier-item.component";
import {NewCustomerSupplierComponent} from "../../components/new-customer-supplier/new-customer-supplier.component";
import {SharedModule} from "../../../shared/shared.module";
import {SharedMemberModule} from "../../shared-member/shared-member.module";


@NgModule({
  declarations: [
    CustomersListComponent,
    SuppliersDeleteComponent,
    CustomersDeleteComponent,
    SuppliersListComponent,
    CustomerSupplierItemComponent,
    NewCustomerSupplierComponent
  ],
    imports: [
        SharedModule,
        CustomersSuppliersRoutingModule,
        SharedMemberModule
    ],
  entryComponents: [
    SuppliersDeleteComponent,
    CustomersDeleteComponent,
  ]
})
export class CustomersSuppliersModule { }
