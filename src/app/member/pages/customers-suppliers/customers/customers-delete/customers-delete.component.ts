import { Component} from '@angular/core';
import {ClientDto} from "../../../../../../gs-api/src/models/client-dto";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {CustomerService} from "../service/customer.service";

@Component({
  selector: 'app-customers-delete',
  templateUrl: './customers-delete.component.html',
  styleUrls: ['./customers-delete.component.scss']
})
export class CustomersDeleteComponent {

  customer: ClientDto = {};
  errorMsg: string = '';
  isVisible = false;

  constructor(
      private activeModal: NgbActiveModal,
      private customerService: CustomerService,
  ) { }

  cancel() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id : number): void{
    this.isVisible = true;
    this.customerService.deleteCustomer(id).subscribe({next: res => {
        this.activeModal.close("success");
        this.isVisible = false;
      }, error: err => {
        this.errorMsg = err.error.error;
        this.isVisible = false;
      }});
  }

}
