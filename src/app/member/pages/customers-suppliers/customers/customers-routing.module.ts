import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CustomersListComponent} from "./customers-list/customers-list.component";
import {NewCustomerSupplierComponent} from "../../../components/new-customer-supplier/new-customer-supplier.component";
import {CustomerResolveService} from "./service/customer-resolve.service";

const routes: Routes = [
  {
    path: '',
    component: CustomersListComponent
  },
  {
    path: 'new',
    data: {
      origin: 'client'
    },
    component: NewCustomerSupplierComponent
  },
  {
    path: ':id/edit',
    data: {
      origin: 'client'
    },
    component: NewCustomerSupplierComponent,
    resolve: {
      clientFournisseur: CustomerResolveService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomersRoutingModule { }
