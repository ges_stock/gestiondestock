import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {ClientDto} from "../../../../../../gs-api/src/models/client-dto";
import {Router} from "@angular/router";
import {CustomerService} from "../service/customer.service";

@Component({
  selector: 'app-customers-list',
  templateUrl: './customers-list.component.html',
  styleUrls: ['./customers-list.component.scss']
})
export class CustomersListComponent implements OnInit {

  customers$: Observable<ClientDto[] | []> | undefined;

  constructor(
      private router: Router,
      private customerService: CustomerService,
  ) { }

  ngOnInit(): void {
    this.refresh();
  }

  newCustomer(): void {
    this.router.navigate(['clients/new']);
  }

  refresh(): void {
    this.customers$ = this.customerService.findAll();
  }

  trackBy(index: number, item: ClientDto): number {
    return item.id!;
  }

  handleDelete(event: any){
    if (event === 'customer-delete') {
      this.refresh();
    }
  }

}
