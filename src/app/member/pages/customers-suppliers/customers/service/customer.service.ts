import { Injectable } from '@angular/core';
import {ClientsService} from "../../../../../../gs-api/src/services/clients.service";
import {ClientDto} from "../../../../../../gs-api/src/models/client-dto";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(
      private clientsService: ClientsService
  ) { }

  save(clientDto: ClientDto): Observable<ClientDto> {
    return this.clientsService.save(clientDto);
  }

  findAll(): Observable<ClientDto[]> {
    return this.clientsService.findAll();
  }

  findById(idClient: number): Observable<ClientDto> {
    return this.clientsService.findById(idClient);
  }

  deleteCustomer(idClient: number): Observable<any> {
    if (idClient) {
      return this.clientsService.delete(idClient);
    }
    return of();
  }

}
