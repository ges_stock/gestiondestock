import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs";
import {mergeMap} from "rxjs/operators";
import {EMPTY} from "rxjs";
import {of} from "rxjs";
import {ClientDto} from "../../../../../../gs-api/src/models";
import {CustomerService} from "./customer.service";


@Injectable({providedIn: 'root'})
export class CustomerResolveService  implements Resolve<ClientDto>{

    constructor(private service: CustomerService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable <ClientDto> | Promise<ClientDto> | ClientDto {
        const id = route.params['id'];
        if (id) {
            return this.service.findById(id).pipe(
                mergeMap((client: ClientDto) => {
                    if (client) {
                        return of(client);
                    }
                    else {
                        return EMPTY;
                    }
                })
            );
        }
        return of({});
    }

}
