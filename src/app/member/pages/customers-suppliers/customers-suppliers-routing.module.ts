import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'clients',
    loadChildren: () => import('./customers/customers-routing.module').then(m => m.CustomersRoutingModule)
  },
  {
    path: 'fournisseurs',
    loadChildren: () => import('./suppliers/suppliers-routing.module').then(m => m.SuppliersRoutingModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomersSuppliersRoutingModule { }
