import { Component} from '@angular/core';
import {ArticleDto} from "../../../../../gs-api/src/models/article-dto";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {ArticleService} from "../service/article.service";

@Component({
  selector: 'app-articles-delete',
  templateUrl: './articles-delete.component.html',
  styleUrls: ['./articles-delete.component.scss']
})
export class ArticlesDeleteComponent {

  article: ArticleDto = {};
  errorMsg: string = '';
  isVisible = false;

  constructor(
      private activeModal: NgbActiveModal,
      private articleService: ArticleService,
  ) { }

  cancel() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id : number): void{
    this.isVisible = true;
    this.articleService.deleteArticle(id).subscribe({next: res => {
        this.activeModal.close("success");
        this.isVisible = false;
      }, error: err => {
        this.errorMsg = err.error.error;
        this.isVisible = false;
      }});
  }

}
