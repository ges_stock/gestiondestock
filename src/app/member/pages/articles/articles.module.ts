import { NgModule } from '@angular/core';

import { ArticlesRoutingModule } from './articles-routing.module';
import { ArticlesListComponent } from './articles-list/articles-list.component';
import { ArticlesNewComponent } from './articles-new/articles-new.component';
import { ArticlesDeleteComponent } from './articles-delete/articles-delete.component';
import {ArticleItemComponent} from "../../components/article-item/article-item.component";
import {SharedMemberModule} from "../../shared-member/shared-member.module";


@NgModule({
  declarations: [
    ArticlesListComponent,
    ArticlesNewComponent,
    ArticlesDeleteComponent,
    ArticleItemComponent
  ],
  imports: [
    SharedMemberModule,
    ArticlesRoutingModule
  ],
  entryComponents: [
    ArticleItemComponent
  ]
})
export class ArticlesModule { }
