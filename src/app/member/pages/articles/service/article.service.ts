import { Injectable } from '@angular/core';
import {ArticlesService} from "src/gs-api/src/services/articles.service";
import {Observable, of} from "rxjs";
import {ArticleDto} from "../../../../../gs-api/src/models/article-dto";

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(
      private articlesService: ArticlesService
  ) { }

  save(article: ArticleDto): Observable<ArticleDto> {
    return this.articlesService.save(article);
  }

  findAllArticles(): Observable<ArticleDto[]> {
    return this.articlesService.findAll();
  }

  findArticleById(idArticle: number): Observable<ArticleDto> {
    return this.articlesService.findById(idArticle);
  }

  deleteArticle(idArticle: number): Observable<any> {
    if (idArticle) {
      return this.articlesService.delete(idArticle);
    }
    return of();
  }

  findArticleByCode(codeArticle: string): Observable<ArticleDto> {
    return this.articlesService.findByCodeArticle(codeArticle);
  }

}
