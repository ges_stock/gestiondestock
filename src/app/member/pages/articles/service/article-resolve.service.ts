import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs";
import {mergeMap} from "rxjs/operators";
import {EMPTY} from "rxjs";
import {of} from "rxjs";
import {ArticleDto} from "../../../../../gs-api/src/models/article-dto";
import {ArticleService} from "./article.service";


@Injectable({providedIn: 'root'})
export class ArticleResolveService  implements Resolve<ArticleDto>{

    constructor(private articleService: ArticleService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable <ArticleDto> | Promise<ArticleDto> | ArticleDto {
        const id = route.params['id'];
        if (id) {
            return this.articleService.findArticleById(id).pipe(
                mergeMap((article: ArticleDto) => {
                    if (article) {
                        return of(article);
                    }
                    else {
                        return EMPTY;
                    }
                })
            );
        }
        return of({});
    }

}
