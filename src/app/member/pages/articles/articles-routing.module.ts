import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CategoriesListComponent} from "../categories/categories-list/categories-list.component";
import {CategoriesCreateComponent} from "../categories/categories-create/categories-create.component";
import {CategorieResolveService} from "../categories/service/categorie-resolve.service";
import {ArticlesListComponent} from "./articles-list/articles-list.component";
import {ArticlesNewComponent} from "./articles-new/articles-new.component";
import {ArticleResolveService} from "./service/article-resolve.service";

const routes: Routes = [
  {
    path: '',
    component: ArticlesListComponent
  },
  {
    path: 'new',
    component: ArticlesNewComponent
  },
  {
    path: ':id/edit',
    component: ArticlesNewComponent,
    resolve: {
      article: ArticleResolveService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticlesRoutingModule { }
