import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {ArticleDto} from "../../../../../gs-api/src/models/article-dto";
import {Router} from "@angular/router";
import {ArticleService} from "../service/article.service";

@Component({
  selector: 'app-articles-list',
  templateUrl: './articles-list.component.html',
  styleUrls: ['./articles-list.component.scss']
})
export class ArticlesListComponent implements OnInit {

  articles$: Observable<ArticleDto[] | []> | undefined;

  constructor(
      private router: Router,
      private articleService: ArticleService
  ) { }

  ngOnInit(): void {
    this.refresh();
  }

    addArticle(): void {
      this.router.navigate(['articles/new']);
    }

    refresh(): void {
      this.articles$ = this.articleService.findAllArticles();
    }

    trackBy(index: number, article: ArticleDto): number {
      return article.id!;
    }

    handleDelete(event: any){
      if (event === 'article-delete') {
        this.refresh();
      }
    }

}
