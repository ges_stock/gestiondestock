import {Component, OnDestroy, OnInit} from '@angular/core';
import {UtilisateurDto} from "../../../../../gs-api/src/models/utilisateur-dto";
import {FormBuilder} from "@angular/forms";
import {AccountService} from "../../../../core/auth/account.service";
import {ActivatedRoute} from "@angular/router";
import {ArticleService} from "../service/article.service";
import {Subscription} from "rxjs";
import {ArticleDto} from "../../../../../gs-api/src/models/article-dto";
import {CategorieDto} from "../../../../../gs-api/src/models/categorie-dto";
import {Observable} from "rxjs";
import {CategorieService} from "../../categories/service/categories.service";
import {PhotosService} from "../../../../../gs-api/src/services";
import SavePhotoParams = PhotosService.SavePhotoParams;

@Component({
  selector: 'app-articles-new',
  templateUrl: './articles-new.component.html',
  styleUrls: ['./articles-new.component.scss']
})
export class ArticlesNewComponent implements OnInit, OnDestroy {

  account: UtilisateurDto | null = {};
  authSubscription: Subscription = new Subscription();

  article: ArticleDto = {};

  categories$: Observable<CategorieDto[] | []> | undefined;
  errorMsg: Array<string> = [];

  file: File | null | undefined;
  urlImg: string | ArrayBuffer | undefined = 'assets/images/default-product.jpg';

  isSaving = false;

  articleForm = this.fb.group({
    codeArticle: [''],
    designation: [''],
    prixUnitaireHt: [0],
    prixUnitaireTtc: [0],
    tauxTva: [0],
    categorie: [0]
  });

  constructor(
      private fb: FormBuilder,
      private articleService: ArticleService,
      private categorieService: CategorieService,
      private accountService: AccountService,
      private photoService: PhotosService,
      private activateRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.authSubscription.add(this.accountService.getAuthenticationState().subscribe(account => {
      this.account = account;
    }));
    this.categories$ = this.categorieService.findAll();
    this.activateRoute.data.subscribe(({article}) => {
      this.article = article;
      this.updateForm(article);
      console.log(this.article);
      console.log(this.articleForm.get('categorie')?.value);
    })
  }

  ngOnDestroy(): void {
    this.authSubscription.unsubscribe();
  }

  previousPage(): void {
    window.history.back();
  }

  create(): void {
    this.isSaving = true;
    const article = this.createForm();

    this.articleService.save(article).subscribe({next: res => {
        this.savePhoto(res.id, res.codeArticle);
      }, error: err => {
        this.errorMsg = err.error.errors;
        this.isSaving = false;
      }});
  }

  private savePhoto(id: number | undefined, codeArticle: string | undefined) {
    if (id && codeArticle && this.file) {
      const params: SavePhotoParams = {
        id: id,
        file: this.file,
        title: codeArticle,
        context: 'article'
      };
      this.photoService.savePhoto(params).subscribe({next: () => {
        this.isSaving = false;
        this.previousPage();
        }});
    }
    else {
      this.previousPage();
    }
  }

  calculTTC(): void {
    this.articleForm.get(['prixUnitaireTtc'])?.setValue(+(this.articleForm.get('prixUnitaireHt')?.value ?? 0) + (+(this.articleForm.get('prixUnitaireHt')?.value ?? 0) * (+(this.articleForm.get('tauxTva')?.value ?? 0) /100)))
  }

  onFileInput(files: FileList | null): void {
    if (files) {
      this.file = files.item(0);
      if (this.file) {
        const fileReader = new FileReader();
        fileReader.readAsDataURL(this.file);
        fileReader.onload = () => {
          if (fileReader.result) {
            this.urlImg = fileReader.result;
          }
        }
      }
    }
  }

  private updateForm(article: ArticleDto): void {
    this.articleForm.patchValue({
      codeArticle: article?.codeArticle,
      designation: article?.designation,
      prixUnitaireHt: article?.prixUnitaireHt,
      tauxTva: article?.tauxTva,
      prixUnitaireTtc: article?.prixUnitaireTtc,
      categorie: article?.categorie?.id
    });
    this.urlImg = article?.photo ?? 'assets/images/default-product.jpg';
  }

  createForm(): ArticleDto {
    return {
      ...this.article,
      designation: this.articleForm.get('designation')?.value ?? '',
      prixUnitaireHt: (this.articleForm.get('prixUnitaireHt')?.value ?? 0),
      prixUnitaireTtc: (this.articleForm.get('prixUnitaireTtc')?.value ?? 0),
      tauxTva: (this.articleForm.get('tauxTva')?.value ?? 0),
      idEntreprise: this.account?.entreprise?.id,
      codeArticle: this.articleForm.get('codeArticle')?.value ?? '',
      categorie: {
        id : Number(this.articleForm.get('categorie')?.value ) ?? null
      }
    }
  }

}
