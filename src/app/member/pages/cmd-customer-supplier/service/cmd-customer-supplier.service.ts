import { Injectable } from '@angular/core';
import {CommandefournisseurService, CommandesclientsService} from "../../../../../gs-api/src/services";
import {Observable} from "rxjs";
import {CommandeClientDto} from "../../../../../gs-api/src/models/commande-client-dto";
import {CommandeFournisseurDto} from "../../../../../gs-api/src/models/commande-fournisseur-dto";
import {LigneCommandeClientDto, LigneCommandeFournisseurDto} from "../../../../../gs-api/src/models";

@Injectable({
  providedIn: 'root'
})
export class CmdCustomerSupplierService {

  constructor(
      private cmdClient: CommandesclientsService,
      private cmdFournisseur: CommandefournisseurService,
  ) { }

  saveOrderCustomer(cmdClientDto: CommandeClientDto): Observable<CommandeClientDto> {
    return this.cmdClient.save(cmdClientDto);
  }

  saveOrderSupplier(cmdFournisseurDto: CommandeFournisseurDto): Observable<CommandeFournisseurDto> {
    return this.cmdFournisseur.save(cmdFournisseurDto);
  }

  findAllOrderCustomer(): Observable<CommandeClientDto[]> {
    return this.cmdClient.findAll();
  }

  findAllOrderSupplier(): Observable<CommandeFournisseurDto[]> {
    return this.cmdFournisseur.findAll();
  }

  findAllLigneOrderCustomer(idCmdCustomer: number): Observable<LigneCommandeClientDto[]> {
    return this.cmdClient.findAllLignesCommandesClientByCommandeClientId(idCmdCustomer);
  }

  findAllLigneOrderSupplier(idCmdSupplier: number): Observable<LigneCommandeFournisseurDto[]> {
    return this.cmdFournisseur.findAllLignesCommandesFournisseurByCommandeFournisseurId(idCmdSupplier);
  }

}
