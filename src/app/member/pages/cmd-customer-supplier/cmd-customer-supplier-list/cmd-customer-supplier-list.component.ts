import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {CmdCustomerSupplierService} from "../service/cmd-customer-supplier.service";

@Component({
  selector: 'app-cmd-customer-supplier-list',
  templateUrl: './cmd-customer-supplier-list.component.html',
  styleUrls: ['./cmd-customer-supplier-list.component.scss']
})
export class CmdCustomerSupplierListComponent implements OnInit, OnDestroy {

  serviceSubcription = new Subscription();
  origin = '';
  orders: Array<any> = [];
  mapLigneOrder = new Map();
  mapPriceTotalOrder = new Map();

  constructor(
      private router: Router,
      private activateRouter: ActivatedRoute,
      private cmdCustomerSupplierService: CmdCustomerSupplierService
  ) { }

  ngOnInit(): void {
    this.getOrigin();
  }

  ngOnDestroy(): void {
    this.serviceSubcription.unsubscribe();
  }

  newOrder(): void {
    const  page = this.getPage();
    if (page != null) {
      this.router.navigate([page]);
    }
  }

  getOrigin(): void {
    this.serviceSubcription.add(this.activateRouter.data.subscribe(data => {
      this.origin = data['origin'];
    }));
  }

  findAll(): void {
    if (this.origin === 'client') {
      this.cmdCustomerSupplierService.findAllOrderCustomer().subscribe({next: res => {
        this.orders = res;
        this.findAllLignesOrder();
      }})
    }
    else if (this.origin === 'fournisseur') {
      this.cmdCustomerSupplierService.findAllOrderSupplier().subscribe({next: res => {
          this.orders = res;
          this.findAllLignesOrder();
        }})
    }
  }

  getPage(): string | null {
    if (this.origin === 'client') {
      return 'commandesclient/nouvellecommande';
    }
    else if (this.origin === 'fournisseur') {
      return 'commandesfournisseur/nouvellecommande';
    }
    return null;
  }

  findAllLignesOrder(): void {
    this.orders.forEach(cmd => {
      this.findLigneOrder(cmd.id);
    })
  }

  findLigneOrder(id: number): void {
    if (this.origin === 'client') {
      this.cmdCustomerSupplierService.findAllLigneOrderCustomer(id).subscribe({next: res => {
        this.mapLigneOrder.set(id, res);
        this.mapPriceTotalOrder.set(id, this.calculerTotalCmd(res));
      }})
    }
    else if (this.origin === 'fournisseur') {
      this.cmdCustomerSupplierService.findAllLigneOrderSupplier(id).subscribe({next: res => {
          this.mapLigneOrder.set(id, res);
          this.mapPriceTotalOrder.set(id, this.calculerTotalCmd(res));
      }})
    }
  }

  calculerTotalCmd(list: Array<any>): number {
    let total = 0;
    list.forEach(ligne => {
      if (ligne.prixUnitaire && ligne.quantite) {
        total += ligne.quantite * +ligne.prixUnitaire;
      }
    });
    return Math.floor(total);
  }

  calculerTotalOrder(id: number): number {
    return this.mapPriceTotalOrder.get(id);
  }

}
