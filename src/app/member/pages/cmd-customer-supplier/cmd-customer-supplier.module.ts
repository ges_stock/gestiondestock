import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CmdCustomerSupplierRoutingModule } from './cmd-customer-supplier-routing.module';
import { CmdCustomerSupplierListComponent } from './cmd-customer-supplier-list/cmd-customer-supplier-list.component';
import {CmdCustomerSupplierItemComponent} from "../../components/cmd-customer-supplier-item/cmd-customer-supplier-item.component";


@NgModule({
  declarations: [
    CmdCustomerSupplierListComponent,
    CmdCustomerSupplierItemComponent
  ],
  imports: [
    CommonModule,
    CmdCustomerSupplierRoutingModule
  ]
})
export class CmdCustomerSupplierModule { }
