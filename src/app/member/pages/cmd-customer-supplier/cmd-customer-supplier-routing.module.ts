import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CmdCustomerSupplierListComponent} from "./cmd-customer-supplier-list/cmd-customer-supplier-list.component";
import {NewCmdCustomerSupplierComponent} from "../../components/new-cmd-customer-supplier/new-cmd-customer-supplier.component";

const routes: Routes = [
  {
    path: '',
    component: CmdCustomerSupplierListComponent
  },
  {
    path: 'nouvellecommande',
    component: NewCmdCustomerSupplierComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CmdCustomerSupplierRoutingModule { }
