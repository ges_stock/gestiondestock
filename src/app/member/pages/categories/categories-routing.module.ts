import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CategoriesListComponent} from "./categories-list/categories-list.component";
import {CategoriesCreateComponent} from "./categories-create/categories-create.component";
import {CategorieResolveService} from "./service/categorie-resolve.service";

const routes: Routes = [
  {
    path: '',
    component: CategoriesListComponent
  },
  {
    path: 'new',
    component: CategoriesCreateComponent
  },
  {
    path: ':id/edit',
    component: CategoriesCreateComponent,
    resolve: {
      categorie: CategorieResolveService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoriesRoutingModule { }
