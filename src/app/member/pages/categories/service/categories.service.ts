import { Injectable } from '@angular/core';
import {CategoriesService} from "src/gs-api/src/services/categories.service";
import {Observable} from "rxjs";
import {CategorieDto} from "../../../../../gs-api/src/models/categorie-dto";
import {of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CategorieService {

  constructor(
      private categoriesService: CategoriesService
  ) { }

  save(categorie: CategorieDto): Observable<CategorieDto> {
    return this.categoriesService.save(categorie);
  }

  findAll(): Observable<CategorieDto[]> {
    return this.categoriesService.findAll();
  }

  findById(idCategorie: number): Observable<CategorieDto> {
    return this.categoriesService.findById(idCategorie);
  }

  delete(idCategorie: number): Observable<any> {
    if (idCategorie) {
      return this.categoriesService.delete(idCategorie);
    }
    return of();
  }

}
