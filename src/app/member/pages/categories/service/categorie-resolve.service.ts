import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {CategorieDto} from "../../../../../gs-api/src/models/categorie-dto";
import {Observable} from "rxjs";
import {CategorieService} from "./categories.service";
import {mergeMap} from "rxjs/operators";
import {EMPTY} from "rxjs";
import {of} from "rxjs";


@Injectable({providedIn: 'root'})
export class CategorieResolveService  implements Resolve<CategorieDto>{

    constructor(private categorieService: CategorieService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable <CategorieDto> | Promise<CategorieDto> | CategorieDto {
        const id = route.params['id'];
        if (id) {
            return this.categorieService.findById(id).pipe(
                mergeMap((categorie: CategorieDto) => {
                    if (categorie) {
                        return of(categorie);
                    }
                    else {
                        return EMPTY;
                    }
                })
            );
        }
        return of({});
    }

}
