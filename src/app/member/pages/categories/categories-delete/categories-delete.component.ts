import { Component, OnInit } from '@angular/core';
import {Categorie} from "../../../../../gs-api/src/models/categorie";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {CategorieService} from "../service/categories.service";
import {Router} from "@angular/router";
import {success} from "ng-packagr/lib/utils/log";

@Component({
  selector: 'app-categories-delete',
  templateUrl: './categories-delete.component.html',
  styleUrls: ['./categories-delete.component.scss']
})
export class CategoriesDeleteComponent implements OnInit {

  categorieToDelete?: Categorie;
  errorMsg: Array<string> = [];

  constructor(
      private activeModal: NgbActiveModal,
      private categorieService: CategorieService,
      private router: Router
  ) { }

  ngOnInit(): void {
  }

  confirmDelete(id : number): void{
    this.categorieService.delete(id).subscribe({next: res => {
        this.activeModal.close("success");
      }, error: err => {
        this.errorMsg = err.error.error;
      }});
  }

  clear() {
    this.activeModal.dismiss('cancel');
  }

  cancel(): void {
    this.router.navigate(['/categories']);
  }

}
