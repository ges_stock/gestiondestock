import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { Observable } from 'rxjs';
import {Categorie, CategorieDto} from 'src/gs-api/src/models';
import { CategorieService } from '../service/categories.service';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {CategoriesDeleteComponent} from "../categories-delete/categories-delete.component";
import {EventManager} from "@angular/platform-browser";

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss']
})
export class CategoriesListComponent implements OnInit {

  categories$: Observable<CategorieDto[] | []> | undefined;

  constructor(
      private router: Router,
      private modalService: NgbModal,
      private eventManager: EventManager,
      private categorieService: CategorieService
  ) { }

  ngOnInit(): void {
    this.categories$ = this.categorieService.findAll();
  }

  addCategorie() {
    this.router.navigate(['categories', 'new']);
  }

  refresh(): void {
    this.categories$ = this.categorieService.findAll();
  }

  identify(index: number, categorie: CategorieDto){
    return categorie.id;
  }

  openModalDeleteCategorie(categorie: any) {
    const modalRef = this.modalService.open(CategoriesDeleteComponent, {size: 'lg', backdrop: 'static'});
    modalRef.componentInstance.categorieToDelete = categorie;
    modalRef.closed.subscribe(res => {
      if (res === "success") {
        this.refresh();
      }
    })
  }

}
