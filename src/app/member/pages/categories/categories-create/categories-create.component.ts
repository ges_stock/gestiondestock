import {Component, OnDestroy, OnInit} from '@angular/core';
import {UtilisateurDto} from "../../../../../gs-api/src/models/utilisateur-dto";
import {Subscription} from "rxjs";
import {CategorieService} from "../service/categories.service";
import {AccountService} from "../../../../core/auth/account.service";
import {ActivatedRoute} from "@angular/router";
import {CategorieDto} from "../../../../../gs-api/src/models/categorie-dto";
import {FormBuilder} from "@angular/forms";

@Component({
  selector: 'app-categories-create',
  templateUrl: './categories-create.component.html',
  styleUrls: ['./categories-create.component.scss']
})
export class CategoriesCreateComponent implements OnInit, OnDestroy {

  account: UtilisateurDto | null = {};

  authSubscription: Subscription = new Subscription();
  categorie: CategorieDto = {};
  errorMsg: Array<string> = [];

  categoryForm = this.fb.group({
    code: [''],
    designation: ['']
  });

  constructor(
      private fb: FormBuilder,
      private categoriesService: CategorieService,
      private accountService: AccountService,
      private activateRoute: ActivatedRoute,
  ) { }

  ngOnDestroy(): void {
    this.authSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.authSubscription.add(this.accountService.getAuthenticationState().subscribe(account => {this.account = account}));
    this.activateRoute.data.subscribe(({categorie}) => {
      this.categorie = categorie;
      this.updateForm(categorie);
    })
  }

  cancel(): void {
    window.history.back();
  }

  create() {
    const categorie = this.createForm();
    this.categoriesService.save(categorie).subscribe({next: res => {
      this.cancel();
    }, error: err => {
      this.errorMsg = err.error.errors;
    }});
  }

  private updateForm(categorie: CategorieDto):void {
    this.categoryForm.patchValue({
      code: categorie.code ?? '',
      designation: categorie.designation ?? '',
    })
  }

  private createForm(): CategorieDto {
    return {
      ...this.categorie,
      code: this.categoryForm.get('code')?.value ?? '',
      designation: this.categoryForm.get('designation')?.value ?? '',
      idEntreprise: this.account?.entreprise?.id
    }
  }

}
