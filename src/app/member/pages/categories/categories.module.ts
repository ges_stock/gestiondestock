import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriesRoutingModule } from './categories-routing.module';
import { CategoriesListComponent } from './categories-list/categories-list.component';
import { CategoriesCreateComponent } from './categories-create/categories-create.component';
import {SharedMemberModule} from "../../shared-member/shared-member.module";
import { CategoriesDeleteComponent } from './categories-delete/categories-delete.component';


@NgModule({
  declarations: [
    CategoriesListComponent,
    CategoriesCreateComponent,
    CategoriesDeleteComponent
  ],
  imports: [
    CommonModule,
    CategoriesRoutingModule,
    SharedMemberModule
  ]
})
export class CategoriesModule { }
