import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Router} from "@angular/router";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {CustomersDeleteComponent} from "../../pages/customers-suppliers/customers/customers-delete/customers-delete.component";
import {SuppliersDeleteComponent} from "../../pages/customers-suppliers/suppliers/suppliers-delete/suppliers-delete.component";

@Component({
  selector: 'app-customer-supplier-item',
  templateUrl: './customer-supplier-item.component.html',
  styleUrls: ['./customer-supplier-item.component.scss']
})
export class CustomerSupplierItemComponent {

  @Input() origin = '';

  @Input() clientFournisseur: any = {};

  @Output() deleteResult = new EventEmitter();

  constructor(
      private router: Router,
      private modalService: NgbModal
  ) { }

  update(): void {
    if (this.origin === 'client') {
      this.router.navigate(['clients', this.clientFournisseur.id, 'edit']);
    }
    else if (this.origin === 'fournisseur') {
      this.router.navigate(['fournisseurs', this.clientFournisseur.id, 'edit']);
    }
  }

  deleteItem(): void {
    if (this.origin === 'client') {
      this.deleteCustomer();
    }
    else if (this.origin === 'fournisseur') {
      this.deleteSupplier();
    }
  }

  deleteCustomer(): void {
    const customerModal = this.modalService.open(CustomersDeleteComponent, {backdrop: 'static'});
    customerModal.componentInstance.customer = this.clientFournisseur;
    customerModal.closed.subscribe(reason => {
      if (reason === 'success') {
        this.deleteResult.emit('customer-delete');
      }
    })
  }

  deleteSupplier(): void {
    const supplierModal = this.modalService.open(SuppliersDeleteComponent, {backdrop: 'static'});
    supplierModal.componentInstance.supplier = this.clientFournisseur;
    supplierModal.closed.subscribe((reason) => {
      if (reason === 'success') {
        this.deleteResult.emit('supplier-delete');
      }
    })
  }

}
