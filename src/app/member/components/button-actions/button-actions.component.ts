import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-button-actions',
  templateUrl: './button-actions.component.html',
  styleUrls: ['./button-actions.component.scss']
})
export class ButtonActionsComponent {

  @Input() isNewShow = true;
  @Input() isExportShow = true;
  @Input() isImportShow = true;

  @Output() newEvent = new EventEmitter();

  btnNew(): void{
    this.newEvent.emit();
  }

}
