import {Component, Input, OnInit} from '@angular/core';
import {ClientDto} from "../../../../gs-api/src/models/client-dto";
import {FournisseurDto} from "../../../../gs-api/src/models/fournisseur-dto";

@Component({
  selector: 'app-cmd-customer-supplier-item',
  templateUrl: './cmd-customer-supplier-item.component.html',
  styleUrls: ['./cmd-customer-supplier-item.component.scss']
})
export class CmdCustomerSupplierItemComponent implements OnInit {

  @Input() origin = '';
  @Input() order: any = {};

  custSup: ClientDto | FournisseurDto | undefined = {};

  constructor() { }

  ngOnInit(): void {
    this.extractCutomerSupplier();
  }

  extractCutomerSupplier(): void {
    if (this.origin === 'client') {
      this.custSup = this.order?.client;
    }
    else if (this.origin === 'fournisseur') {
      this.custSup = this.order?.fournisseur;
    }
  }

}
