import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ArticleDto} from "../../../../gs-api/src/models/article-dto";
import {Router} from "@angular/router";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ArticlesDeleteComponent} from "../../pages/articles/articles-delete/articles-delete.component";

@Component({
  selector: 'app-article-item',
  templateUrl: './article-item.component.html',
  styleUrls: ['./article-item.component.scss']
})
export class ArticleItemComponent {

  @Input()
  article: ArticleDto = {};

  @Output() deleteArticle = new EventEmitter();

  @Output() updateArticle = new EventEmitter();

  constructor(
      private router: Router,
      private modalService: NgbModal,
  ) { }

  update(): void {
    this.router.navigate(['articles', this.article.id, 'edit']);
  }

  delete(): void {
    const deleteModal = this.modalService.open(ArticlesDeleteComponent, {backdrop: 'static'});
    deleteModal.componentInstance.article = this.article;
    deleteModal.closed.subscribe(reason => {
      if (reason === 'success') {
        this.deleteArticle.emit('article-delete');
      }
    });

  }

}
