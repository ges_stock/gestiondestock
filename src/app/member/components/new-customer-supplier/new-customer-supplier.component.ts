import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {UtilisateurDto} from "../../../../gs-api/src/models/utilisateur-dto";
import {AccountService} from "../../../core/auth/account.service";
import {PhotosService} from "../../../../gs-api/src/services/photos.service";
import SavePhotoParams = PhotosService.SavePhotoParams;
import {CustomerService} from "../../pages/customers-suppliers/customers/service/customer.service";
import {SupplierService} from "../../pages/customers-suppliers/suppliers/service/supplier.service";
import {ActivatedRoute} from "@angular/router";
import {FormBuilder} from "@angular/forms";
import {ClientDto} from "../../../../gs-api/src/models/client-dto";
import {FournisseurDto} from "../../../../gs-api/src/models/fournisseur-dto";

@Component({
  selector: 'app-new-customer-supplier',
  templateUrl: './new-customer-supplier.component.html',
  styleUrls: ['./new-customer-supplier.component.scss']
})
export class NewCustomerSupplierComponent implements OnInit, OnDestroy {

  serviceSubscription = new Subscription();
  origin = '';
  pathUrl = '';

  account: UtilisateurDto | null = {};

  clientFournisseur: any = {};
  errorsMsg: Array<string> = [];
  file: File | null | undefined;
  urlImg: string | ArrayBuffer | undefined = 'assets/images/avatar.png';
  isSaving = false;

  clientFournisseurForm= this.fb.group({
    nom: [''],
    prenom: [''],
    numTel: [''],
    adresse: this.fb.group({
      adresse1: [''],
      adresse2: [''],
      codePostale: [''],
      pays: [''],
      ville: ['']
    }),
    mail: ['']
  });

  constructor(
      private accountService: AccountService,
      private photoService: PhotosService,
      private customerService: CustomerService,
      private supplierService: SupplierService,
      private activatedRoute: ActivatedRoute,
      private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.serviceSubscription.add(this.accountService.getAuthenticationState().subscribe(account => {
      this.account = account;
    }));
    this.serviceSubscription.add(this.activatedRoute.data.subscribe(data => {
      this.origin = data['origin'];
      this.setPathUrl(this.origin);
      this.clientFournisseur = data['clientFournisseur'];
      this.updateForm(this.clientFournisseur);
    }));
  }

  save(): void {
    this.isSaving = true;
    if (this.origin === 'client') {
      window.console.log(this.mapToCustomer());
      this.customerService.save(this.mapToCustomer()).subscribe({
        next: res => {
          this.savePhoto(res.id, res.nom);
        },
        error: err => {
          this.isSaving = false;
          this.errorsMsg = err.error.errors;
        }
      });
    }
    else if (this.origin === 'fournisseur') {
      window.console.log(this.mapToSupplier());
      this.supplierService.save(this.mapToSupplier()).subscribe({
        next: res => {
          this.savePhoto(res.id, res.nom);
        },
        error: err => {
          this.isSaving = false;
          this.errorsMsg = err.error.errors;
        }
      });
    }
  }

  setPathUrl(origin: string): void {
    if (origin === 'client') {
      this.pathUrl = 'clients';
    }
    else if (origin === 'fournisseur') {
      this.pathUrl = 'fournisseurs';
    }
  }

  mapToCustomer(): ClientDto {
    const customer: ClientDto = this.creatForm();
    return customer;
  }

  mapToSupplier(): FournisseurDto {
    const supplier: ClientDto = this.creatForm();
    return supplier;
  }

  ngOnDestroy(): void {
    this.serviceSubscription.unsubscribe();
  }

  previousPage(): void {
    window.history.back();
  }

  private savePhoto(id: number | undefined, title: string | undefined) {
    if (id && title && this.file) {
      const params: SavePhotoParams = {
        id: id,
        file: this.file,
        title: title,
        context: this.origin
      };
      this.photoService.savePhoto(params).subscribe({
        next: () => {
          this.isSaving = false;
          this.previousPage();
        }
      });
    } else {
      this.isSaving = false;
      this.previousPage();
    }
  }

  onFileInput(files: FileList | null): void {
    if (files) {
      this.file = files.item(0);
      if (this.file) {
        const fileReader = new FileReader();
        fileReader.readAsDataURL(this.file);
        fileReader.onload = () => {
          if (fileReader.result) {
            this.urlImg = fileReader.result;
          }
        }
      }
    }
  }

  updateForm(clientFournisseur: any): void {
    this.clientFournisseurForm.patchValue({
      nom: clientFournisseur?.nom,
      prenom: clientFournisseur?.prenom,
      numTel: clientFournisseur?.numTel,
      mail: clientFournisseur?.mail,
      adresse: {
        adresse1: clientFournisseur?.adresse?.adresse1,
        adresse2: clientFournisseur?.adresse?.adresse2,
        codePostale: clientFournisseur?.adresse?.codePostale,
        pays: clientFournisseur?.adresse?.pays,
        ville: clientFournisseur?.adresse?.ville
      }
    });
    this.urlImg = clientFournisseur?.photo ?? 'assets/images/avatar.png';
  }

  creatForm(): any {
    return {
      ...this.clientFournisseur,
      nom: this.clientFournisseurForm.get('nom')?.value,
      prenom: this.clientFournisseurForm.get('prenom')?.value,
      numTel: this.clientFournisseurForm.get('numTel')?.value,
      mail: this.clientFournisseurForm.get('mail')?.value,
      adresse: {
        adresse1: this.clientFournisseurForm.get('adresse.adresse1')?.value,
        adresse2: this.clientFournisseurForm.get('adresse.adresse2')?.value,
        codePostale: this.clientFournisseurForm.get('adresse.codePostale')?.value,
        pays: this.clientFournisseurForm.get('adresse.pays')?.value,
        ville: this.clientFournisseurForm.get('adresse.ville')?.value
      },
      idEntreprise: this.account?.entreprise?.id
    }

  }

}
