import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {layoutsRouters} from "./layouts/layouts.route";

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import("./pages/dashboard/dashboard.module").then(m => m.DashboardModule)
  },
  {
    path: '',
    loadChildren: () => import("./pages/customers-suppliers/customers-suppliers.module").then(m => m.CustomersSuppliersModule)
  },
  {
    path: 'categories',
    loadChildren: () => import("./pages/categories/categories.module").then(m => m.CategoriesModule)
  },
  {
    path: 'articles',
    loadChildren: () => import("./pages/articles/articles.module").then(m => m.ArticlesModule)
  },
  {
    path: 'commandesclient',
    data: {
      origin: 'client'
    },
    loadChildren: () => import("./pages/cmd-customer-supplier/cmd-customer-supplier.module").then(m => m.CmdCustomerSupplierModule)
  },
  {
    path: 'commandesfournisseur',
    data: {
      origin: 'fournisseur'
    },
    loadChildren: () => import("./pages/cmd-customer-supplier/cmd-customer-supplier.module").then(m => m.CmdCustomerSupplierModule)
  },
  ...layoutsRouters,
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MemberRoutingModule { }
