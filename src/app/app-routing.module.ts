import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MemberComponent} from "./member/member.component";
import {UserRouterAccessService} from "./core/auth/user-router-access.service";

const routes: Routes = [
  {
    path: '',
    component: MemberComponent,
    canActivate: [UserRouterAccessService],
    loadChildren: () => import('./member/member.module').then(m => m.MemberModule),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
